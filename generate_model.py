import os
import pandas as pd
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import make_pipeline
from lime.lime_text import LimeTextExplainer
from sklearn.feature_extraction.text import TfidfVectorizer
import re
from sklearn.externals import joblib
from sapient_classifier.lib.misc import normalize, replaceNumbers

cwd = os.getcwd()

training_set_file_path = os.path.join(cwd, 'sapient_classifier/static/datasets/test2.csv')
validation_set_file_path = os.path.join(cwd, 'sapient_classifier/static/datasets/validation.csv')
model_file_path = os.path.join(cwd, 'sapient_classifier/static/objects/model')
vectorizer_file_path = os.path.join(cwd, 'sapient_classifier/static/objects/vectorizer')

trainSet=pd.read_csv(training_set_file_path, encoding='iso-8859-1')
trainSet['FullText']=trainSet['Subject']+' '+trainSet['Message']
trainSet['flag']=1
testSet=pd.read_csv(validation_set_file_path,encoding='iso-8859-1')
testSet=testSet[['Subject','Message']]
testSet['flag']=0
testSet['FullText']=testSet['Subject']+' '+testSet['Message']

FullData= pd.concat([trainSet,testSet])
FullData['FullText']=FullData['FullText'].map(lambda x: x.replace(',',''))
FullData['FullText']=FullData['FullText'].map(lambda x: x.replace('.',''))
FullData['FullText']=FullData['FullText'].map(lambda x: replaceNumbers(x))

FullData=FullData[~FullData['Class'].isnull()]

vectorizer = TfidfVectorizer(use_idf=True,tokenizer=normalize,stop_words='english',ngram_range=(1,2),min_df=3)
full_vectors = vectorizer.fit_transform(FullData.FullText) 

joblib.dump(vectorizer, vectorizer_file_path)

nb = MultinomialNB(alpha=.01)
nb.fit(full_vectors, FullData.Class)

joblib.dump(nb, model_file_path)
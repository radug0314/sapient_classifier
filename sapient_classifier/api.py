import sys

from flask import request, jsonify
from flask_cors import cross_origin
import numpy as np
import pandas as pd

from sapient_classifier import app
from .engines.binary_classifier import BinaryClassifierEngine
from .engines.email_classifier import EmailClassifierEngine
from .lib.file_management import static_path

ece = EmailClassifierEngine(static_path('objects/model'), static_path('objects/vectorizer'))
bce = BinaryClassifierEngine(static_path('objects/binary'))

@app.route('/api/email/predict', methods=['POST'])
@cross_origin()
def email_predict():
	if 'target_file' not in request.files:
		return jsonify({'error': 'File not found'})

	raw_resp = ece.run(stream=request.files['target_file']).to_dict()

	resp = []

	for i in range(0, len(raw_resp['Subject'])):
		email = {}
		email['subject'] = raw_resp['Subject'][i]
		email['body'] = raw_resp['Message'][i]
		email['prediction'] = raw_resp['Predictions'][i]

		resp.append(email)

	return jsonify(resp)

@app.route('/api/email/explain/<id>', methods=['GET'])
def email_explain(id):
	return jsonify(ece.explain_prediction(int(id)))

@app.route('/api/cifd/predict', methods=['POST'])
@cross_origin()
def cifd_predict():
	if 'target_file' not in request.files:
		return jsonify({'error': 'File not found'})

	raw_response = bce.run(stream=request.files['target_file'])
	resp = [{'name': p[0], 'default_prob': 1-p[1], 'data': p[2]} for p in raw_response]

	return jsonify(resp)

@app.route('/api/cifd/explain/<id>')
@cross_origin()
def cifd_explain(id):
	e = bce.explain_prediction(int(id))

	ret = []

	for i in range(len(e['Weight'])):
		ret.append({'feature': e.iloc[i]['Feature'], 'weight': float(e.iloc[i]['Weight']), 'value': float(e.iloc[i]['Value'])})

	return jsonify(ret)

@app.route('/api/cifd/features')
@cross_origin()
def get_features():
	features = [{'name': n} for n in bce.get_features()]

	return jsonify(features)

@app.route('/api/cifd/sens/<feature>')
@cross_origin()
def analyse_sensitivity(feature):
	sa = bce.analyse_sensitivity(feature)

	return jsonify(sa)

@app.route('/api/analytics/histogram', methods=['POST'])
@cross_origin()
def histogram():
	if 'target_file' not in request.files:
		return jsonify({'error': 'File not found'})

	data = pd.read_csv(request.files['target_file'])

	try:
		data = data.drop(['FullName'], axis=1)
	except Exception:
		pass

	# Drop non-numeric features
	features_to_drop = [f for f in list(data.columns) if min(data[f]) in [0, 1] and max(data[f]) in [0, 1]]

	data = data.drop(features_to_drop, axis=1)

	res = {}

	for f in list(data.columns):
		count, division = np.histogram(data[f], bins=10)
		res[f] = list(zip(count.tolist(), division.tolist()))

	return jsonify(res)


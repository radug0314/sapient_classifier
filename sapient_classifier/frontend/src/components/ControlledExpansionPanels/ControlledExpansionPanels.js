import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import ExpansionPanel, {
  ExpansionPanelDetails,
  ExpansionPanelSummary,
} from 'material-ui/ExpansionPanel';
import Typography from 'material-ui/Typography';
import ExpandMoreIcon from 'material-ui-icons/ExpandMore';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import SimpleModal from '../simpleModal/simpleModal';
import Badge from 'material-ui/Badge';
import Divider from 'material-ui/Divider';
import axios from 'axios';
import CustomShapeBarChart from '../customShapeBarChart/customShapeBarChart'
import {emailData} from '../../staticData/response.js';
import {convertSetIntoObj} from '../../utils/helperFunctions.js'
import './ControlledExpansionPanels.css';
import Spinner from '../spinner/spinner';


const styles = theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(13),
    textAlign: 'left'
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(13),
    color: theme.palette.text.secondary,
  },
  icon: {
    verticalAlign: 'bottom',
    height: 20,
    width: 20,
  },
  details: {
    alignItems: 'center',
  },
  column: {
    flexBasis: '33.3%',
  },
  helper: {
    borderLeft: `2px solid ${theme.palette.divider}`,
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`,
  },
  link: {
    color: theme.palette.primary.main,
    textDecoration: 'none',
    '&:hover': {
      textDecoration: 'underline',
    },
  },
  margin: {
    margin: theme.spacing.unit * 2,
  },
  padding: {
    padding: `0 ${theme.spacing.unit * 2}px`,
  },
});


class ControlledExpansionPanels extends React.Component {
  state = {
    expanded: null,
    spinners: {},
    data: {}
  };

  handleChange = panel => (event, expanded) => {

    var self = this;

    this.setState({
      expanded: expanded ? panel : false,
    });

    if (expanded && ! self.state.data.hasOwnProperty(panel)) {
        // Start Spinner
        self.state.spinners[panel] = true

        // Make post request
        fetch('http://localhost:5000/api/email/explain/' + panel.replace("panel", ""), {
          method: 'GET'
        }).then(function(response){
          return response.json();
        }).then(function(r){
          // Stop spinner
          var spinners = self.state.spinners
          spinners[panel] = false

          var data = self.state.data
          data[panel] = r

          self.setState({
            spinners: spinners,
            data: data
          })

          self.state.data[panel] = r
        });
    }
  };

  renderMapResults(classes, expanded, data) {
    let arr = []
    Object.keys(this.props.data.Subject).map((key, index) => {
      arr.push(
        <ExpansionPanel expanded={expanded === 'panel'+index} onChange={this.handleChange('panel'+index)}>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <div className={classes.column}>
                <Typography className={classes.heading}>{this.props.data.Subject[index]}</Typography>
              </div>
              <div className={classes.column}>
                <Typography className={classes.secondaryHeading}>{this.props.data.Predictions[index]}</Typography>
              </div>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <div className='panel-details'>
                <Typography type='caption' align='left'>
                {this.props.data.Message[index]}
                <br/><br />
                </Typography>
                <Divider />
                {this.state.spinners.hasOwnProperty('panel'+index) && this.state.spinners['panel'+index] == true ? <Spinner /> : null}
                {!data.hasOwnProperty('panel'+index) ? null : 
                  <div className='more-info-container'>
                    <div className="graph">
                      <Typography type='body2' align='left' className='sub-header'>Weights for each word:</Typography>
                      <CustomShapeBarChart data={data['panel'+index]}/>
                    </div>
                </div>
              }
                <SimpleModal />
              </div>
            </ExpansionPanelDetails>
          </ExpansionPanel>
      )
    })
    return arr;
  }

  render() {
    const { classes } = this.props;
    const { expanded } = this.state;
    const { data } = this.state;

    console.log(data)

    return (
      <div className='results-component'>
        <MuiThemeProvider className={classes.root}>
          {this.renderMapResults(classes, expanded, data)}
        </MuiThemeProvider>
      </div>
    );
  }
}

ControlledExpansionPanels.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ControlledExpansionPanels);



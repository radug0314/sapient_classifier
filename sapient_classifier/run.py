from . import app
from flask import render_template, send_from_directory

@app.route('/<path:filename>')  
def send_file(filename):  
    return send_from_directory(app.static_folder, filename)

@app.route('/')
def root():
	return render_template('index2.html')

if __name__ == '__main__':
	app.run()
import os

from sapient_classifier import app

def __root_path():
	return app.instance_path

def static_path(name=None):
	return os.path.join(__root_path(), 'static/', name)

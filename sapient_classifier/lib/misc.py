import re
import string

import nltk
from nltk.corpus import wordnet
from nltk.tokenize import sent_tokenize, word_tokenize

stemmer = nltk.stem.porter.PorterStemmer()
remove_punctuation_map = dict((ord(char), 32) for char in string.punctuation)

def stem_tokens(tokens):
    return [stemmer.stem(item) for item in tokens]
    
def replaceNumbers(doc=None):   
    match=re.findall('\d+', doc)
    for m in match:
        doc=re.sub(m, 'N'+str(len(m)), doc)
    return doc    

def normalize(text):
    k= stem_tokens(nltk.word_tokenize(text.lower().translate(remove_punctuation_map)))
    return k
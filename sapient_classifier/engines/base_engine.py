import re

from lime.lime_text import LimeTextExplainer
import pandas as pd
from sklearn.externals import joblib
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline, make_pipeline

class Engine:
    def __init__(self, model_file_path=None):
        self._model = None
        self._model_file_path = model_file_path

        self._model = joblib.load(model_file_path)

    def run(self, stream=None):
        data = self.gather_data(stream)

        processed_data = self.process_data(data)

        return self.apply_model(processed_data)

    def gather_data(self, stream=None):
        raise NotImplementedError

    def process_data(self, data=None):
        raise NotImplementedError

    def apply_model(self, data=None):
        raise NotImplementedError
import re

import pandas as pd
from lime.lime_text import LimeTextExplainer
from sklearn.pipeline import Pipeline, make_pipeline
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.externals import joblib

from .base_engine import Engine

class EmailClassifierEngine(Engine):
    class State:
        def __init__(self, pipeline=None, data=None):
            self.class_names = ['Account closures', 'Credit and account statements', 'Direct debit and Bacs']
            self.lte = LimeTextExplainer(class_names=self.class_names)
            self.pipeline = pipeline
            self.data = data

    def __init__(self, model_file_path=None, vectorizer_file_path=None):
        super(EmailClassifierEngine, self).__init__(model_file_path=model_file_path)

        self._vectorizer = joblib.load(vectorizer_file_path)

        self.state = EmailClassifierEngine.State()

    def gather_data(self, stream=None):
        return pd.read_csv(stream, encoding='iso-8859-1')

    def process_data(self, data=None):
        data = data[['Subject','Message']]
        data['flag'] = 0
        data['FullText'] = data['Subject'] + ' ' + data['Message']
        data['FullText'] = data['FullText'].map(lambda x: x.replace(',',''))
        data['FullText'] = data['FullText'].map(lambda x: x.replace('.',''))
        data['FullText'] = data['FullText'].map(lambda x: EmailClassifierEngine.replace_numbers(x))

        return data
       
    def apply_model(self, data=None):
        # pipeline = Pipeline([('vect', self._vectorizer), ('tfidf', TfidfTransformer()), ('clf', self._model)])
        pipeline = make_pipeline(self._vectorizer, self._model)
        
        vectors = self._vectorizer.transform(data['FullText'])

        data['Predictions'] = self._model.predict(vectors)

        self.state.pipeline = pipeline
        self.state.data = data

        return data

    def explain_prediction(self, index=None):
        data = self.state.data
        lte = self.state.lte
        pipeline = self.state.pipeline
        class_names = self.state.class_names

        if self.state.data is None:
            return {'error': 'Predictions not set'}

        if len(self.state.data) < index:
            return {'error': 'Index invalid'}

        classno = data['Predictions'].map(lambda x: class_names.index(x))

        return (lte.explain_instance(data.FullText[index], pipeline.predict_proba, num_features=6, top_labels=3)
                    .as_list(label=classno.ix[index]))

    @staticmethod
    def replace_numbers(text=None):   
        match = re.findall('\d+', text)
        for m in match:
            text = re.sub(m, 'N%s' % (len(m)), text)
        
        return text
from multiprocessing import cpu_count
from threading import Thread

import numpy as np
import pandas as pd
from sklearn.externals import joblib
from sklearn.linear_model import Ridge
from scipy.spatial import distance

from .base_engine import Engine

class BinaryClassifierEngine(Engine):
    class State:
        def __init__(self, data=None):
            self.data = data

    @staticmethod
    def find_similar(list1, df_sample1,df_sample2,in_col_names,out_col_names,n):
        df_sample11 = df_sample1[in_col_names].copy()
        df_sample21 = df_sample2[in_col_names].copy()
        tuples1 = [tuple(x) for x in df_sample11.values]
        tuples2 = [tuple(y) for y in df_sample21.values]
        cols = out_col_names
        result_df = pd.DataFrame()
        for tup1,row1 in zip(tuples1,df_sample1.iterrows()):
            for tup2,row2 in zip(tuples2,df_sample2.iterrows()):
                list1.append([row1[1][0],row2[1][0],float("{0:.2f}".format(distance.euclidean(tup1,tup2)))])


    def __init__(self, model_file_path=None):
        self._model = None
        self._model_file_path = model_file_path

        self._model = joblib.load(model_file_path)
        self.state = BinaryClassifierEngine.State()

    def gather_data(self, stream=None):
        return pd.read_csv(stream, encoding='iso-8859-1')

    def process_data(self, data=None):
        return data.drop(['SeriousDlqin2yrs'], axis=1)

    def apply_model(self, data=None):
        names = data['FullName'].values
        data = data.drop(['FullName'], axis=1)

        # Get list of probabilities to default
        predictions = self._model.predict_proba(data)[:, 0]

        self.state.data = data

        # Return [(name, probability), ...]
        return list(zip(names, predictions, data.to_dict(orient='records')))

    def __get_similarities(self, df_sample1, df_sample2, in_col_names, out_col_names, n):
        cores = cpu_count()
        list1 = []
        threads = []

        for t in range(cores):
            chunk = len(df_sample2) // cores + 1
            start = t * chunk
            end = ((t + 1) * chunk)
            sl = df_sample2[start:end]

            # Create and start thread
            thr = Thread(target=self.__class__.find_similar, args=(list1, df_sample1, sl, in_col_names, out_col_names, n))
            threads.append(thr)
            thr.start()
        
        for t in range(cores):
            # Wait for threads to return
            threads[t].join()
        
        cols = out_col_names
        result_df = pd.DataFrame()
        result_df = result_df.append(pd.DataFrame(list1, columns=cols),ignore_index=True)
        return result_df.sort_values([cols[2]],ascending=True).groupby(cols[0]).head(n).reset_index(drop=True)

    def explain_prediction(self, id=None):
        # Get row from given data by id
        needle = self.state.data[id:id+1]

        col_names = list(needle.columns)
        out_col_names = list(['FullName', 'Similar_ID', 'Similarity_Index'])
        in_col_names = [x for x in col_names if x not in out_col_names]
        
        n = 100

        similar_entries = self.__get_similarities(needle, self._model.dataset, in_col_names, out_col_names, n)

        # Join data
        similar_entries = pd.merge(left=similar_entries,right=self._model.dataset, left_on='Similar_ID', right_on='ID').drop(['Similar_ID'], axis=1)

        y = similar_entries['Global_prob_nondefault']
        x = similar_entries.drop(['Global_prob_nondefault', 'FullName', 'ID', 'Similarity_Index', 'SeriousDlqin2yrs'], axis=1)
        
        clf = Ridge()
        clf.fit(x.as_matrix(), y.as_matrix())

        clf.predict(needle)


        columns = list(x.columns)

        vals = [needle[v].values[0] for v in columns]

        coefficients = clf.coef_ * vals

        t = zip(columns, coefficients)
        r = pd.DataFrame.from_records(list(t), columns=['Feature', 'Weight'])
        r = r.reindex(r['Weight'].abs().sort_values(ascending=False).index).head(6)

        feature_values = [needle[v].values[0] for v in r['Feature']]
        r['Value'] = feature_values

        return r

    def get_features(self):
        features_to_drop = ['SeriousDlqin2yrs', 'Global_prob_nondefault', 'ID']

        dataset = self._model.dataset.drop(features_to_drop, axis=1)

        # Remove potential boolean features
        features_to_drop = [f for f in dataset.columns if min(dataset[f]) in [0, 1] and max(dataset[f]) in [0, 1]]
        dataset = dataset.drop(features_to_drop, axis=1)

        return list(dataset.columns)

    def _predict_with_fixed_feature(self, ds, feature, vsel, vals):
        res = []
        df = pd.DataFrame(data=None, columns=ds.columns)
        df_data = [vsel(ds[f]) for f in list(ds.columns)]
        df.loc[0] = df_data

        for v in vals:
            df[feature] = [v]
            prediction = self._model.predict_proba(df)[:, 0]
            res.append(prediction[0])

        return res

    def analyse_sensitivity(self, feature, steps=100):
        ds = self._model.dataset.drop(['SeriousDlqin2yrs', 'Global_prob_nondefault', 'ID'], axis=1)

        feature_data = ds[feature]

        # Minimum value of feature
        fminval = min(feature_data)

        # Maximum value of feature
        fmaxval = max(feature_data)

        # Set of feature values
        vals = np.linspace(fminval, fmaxval, num=steps)

        select_min = lambda x: min(x)
        select_max = lambda x: max(x)
        select_quant = lambda x: np.percentile(x, 25)
        select_tquant = lambda x: np.percentile(x, 75)
        select_mean = lambda x: np.mean(x)

        # Reference points for feature
        feature_data = {
            'quant': round(select_quant(feature_data), 3),
            'tquant': round(select_tquant(feature_data), 3),
            'mean': round(select_mean(feature_data), 3)
        }

        for val in feature_data.values():
            if val not in vals:
                vals = np.append(vals, [val])

        vals.sort()

        raw_results = {
            'min': self._predict_with_fixed_feature(ds, feature, select_min, vals),
            'max': self._predict_with_fixed_feature(ds, feature, select_max, vals),
            'quant': self._predict_with_fixed_feature(ds, feature, select_quant, vals),
            'tquant': self._predict_with_fixed_feature(ds, feature, select_tquant, vals),
            'mean': self._predict_with_fixed_feature(ds, feature, select_mean, vals)
        }

        res = {
            'feature_data': feature_data,
            'data': []
        }

        for v in range(len(vals)):
            e = {
                'val': round(vals[v], 3),
                'min': round(raw_results['min'][v], 2),
                'max': round(raw_results['max'][v] , 2),
                'quant': round(raw_results['quant'][v], 2),
                'tquant': round(raw_results['tquant'][v], 2),
                'mean': round(raw_results['mean'][v], 2)
            }

            res['data'].append(e)


        return res


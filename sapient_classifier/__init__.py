from flask import Flask
from flask_cors import CORS
import os

instance_path = os.path.dirname(os.path.abspath(__file__))
static_url = os.path.join(instance_path, 'static/frontend')

if os.name == 'nt':
	instance_path = '//?/%s' % (instance_path)
	static_url = '//?/%s' % (static_url)

app = Flask(__name__, instance_path=instance_path, static_url_path=static_url)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

import sapient_classifier.api
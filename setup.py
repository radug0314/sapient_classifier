import setuptools

from setuptools import setup

setup(
    name='sapient_classifier',
    version='1.0.0',
    author='Sapient',
    packages=['sapient_classifier'],
    package_dir={'sapient_classifier': './sapient_classifier'},
    install_requires=[],
    zip_safe=False
)